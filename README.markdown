Django Basic Tumblelog
================================================

- [Homepage](http://code.google.com/p/django-basic-tumblelog/)

A simple tumblelog application for Django projects.

To install this app, simply create a folder somewhere in your PYTHONPATH place the 'tumblelog' app inside. Then add 'tumblelog' to your projects INSTALLED_APPS list in your settings.py file.

Dependancies
------------

- [Django Tagging](http://code.google.com/p/django-tagging)
- [Pygments](http://pygments.org/)
- [Python Imaging Library (PIL)](http://www.pythonware.com/products/pil/)
