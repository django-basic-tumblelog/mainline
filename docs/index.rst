.. Django Basic Tumblelog documentation master file, created by sphinx-quickstart on Mon Mar  9 14:25:37 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Django Basic Tumblelog
======================

Contents:

.. toctree::
	:maxdepth: 2
	
	tutorials/index
	ref/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

